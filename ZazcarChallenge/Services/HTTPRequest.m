//
//  HTTPRequest.m
//  ZazcarChallenge
//
//  Created by Gabriel Andreatto on 2/19/16.
//  Copyright © 2016 Gabriel Andreatto. All rights reserved.
//

#import "HTTPRequest.h"
#import "Constants.h"
#import <AFNetworking/AFNetworking.h>


@implementation HTTPRequest

- (void) makeGETRequestWithStringUrl: (NSString *) url parameters:(NSDictionary *) parameters completionHandler: (void (^)(id)) completionHandler {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod: @"GET"
                                                  URLString: [HOST_URL stringByAppendingString:url]
                                                 parameters: parameters
                                                      error: nil];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            completionHandler(responseObject);
        }
    }];
    
    [dataTask resume];
}

@end
