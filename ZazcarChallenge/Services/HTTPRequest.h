//
//  HTTPRequest.h
//  ZazcarChallenge
//
//  Created by Gabriel Andreatto on 2/19/16.
//  Copyright © 2016 Gabriel Andreatto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTTPRequest : NSObject

- (void) makeGETRequestWithStringUrl: (NSString *) url parameters:(NSDictionary *) parameters completionHandler: (void (^)(id)) completionHandler;
    
@end
