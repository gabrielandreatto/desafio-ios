//
//  RepositoryModel.h
//  ZazcarChallenge
//
//  Created by Gabriel Andreatto on 2/20/16.
//  Copyright © 2016 Gabriel Andreatto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RepositoryModel : NSObject

@property (nonatomic, strong) NSURL *userAvatar;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *repo_name;
@property (nonatomic, strong) NSString *repo_full_name;
@property (nonatomic, strong) NSString *repo_description;
@property (nonatomic, strong) NSString *forks_count;
@property (nonatomic, strong) NSString *stargazers_count;

@end
