
//
//  RepositoryArrayModel.m
//  ZazcarChallenge
//
//  Created by Gabriel Andreatto on 2/20/16.
//  Copyright © 2016 Gabriel Andreatto. All rights reserved.
//

#import "RepositoryArrayModel.h"
#import "RepositoryModel.h"
#import <Motis/Motis.h>

@implementation RepositoryArrayModel

+ (NSDictionary*) mts_mapping {
    return @{ @"items" : mts_key(repositoryArray)
              };
}

+ (NSDictionary*) mts_arrayClassMapping {
    return @{mts_key(repositoryArray): RepositoryModel.class};
}

@end
