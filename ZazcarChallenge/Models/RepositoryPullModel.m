//
//  RepositoryPullModel.m
//  ZazcarChallenge
//
//  Created by Gabriel Andreatto on 2/21/16.
//  Copyright © 2016 Gabriel Andreatto. All rights reserved.
//

#import "RepositoryPullModel.h"
#import <Motis/Motis.h>

@implementation RepositoryPullModel

+ (NSDictionary*) mts_mapping {
    return @{ @"user.avatar_url" : mts_key(userAvatar),
              @"user.login" : mts_key(username),
              @"title" : mts_key(pull_request_name),
              @"body" : mts_key(pull_request_description),
              @"html_url" : mts_key(pull_request_url)
              };
}

@end
