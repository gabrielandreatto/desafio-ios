//
//  RepositoryPullModel.h
//  ZazcarChallenge
//
//  Created by Gabriel Andreatto on 2/21/16.
//  Copyright © 2016 Gabriel Andreatto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RepositoryPullModel : NSObject

@property (nonatomic, strong) NSURL *userAvatar;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *pull_request_name;
@property (nonatomic, strong) NSString *pull_request_description;
@property (nonatomic, strong) NSURL *pull_request_url;

@end
