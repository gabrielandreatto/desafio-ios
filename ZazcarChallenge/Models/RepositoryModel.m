//
//  RepositoryModel.m
//  ZazcarChallenge
//
//  Created by Gabriel Andreatto on 2/20/16.
//  Copyright © 2016 Gabriel Andreatto. All rights reserved.
//

#import "RepositoryModel.h"
#import <Motis/Motis.h>

@implementation RepositoryModel

+ (NSDictionary*) mts_mapping {
    return @{ @"owner.avatar_url" : mts_key(userAvatar),
              @"owner.login" : mts_key(username),
              @"name" : mts_key(repo_name),
              @"full_name" : mts_key(repo_full_name),
              @"description" : mts_key(repo_description),
              @"forks_count" : mts_key(forks_count),
              @"stargazers_count" : mts_key(stargazers_count)
              };
}


@end
