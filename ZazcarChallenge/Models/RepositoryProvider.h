//
//  RepositoryProvider.h
//  ZazcarChallenge
//
//  Created by Gabriel Andreatto on 2/19/16.
//  Copyright © 2016 Gabriel Andreatto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RepositoryProvider : NSObject

- (void) getRepositoryListWithPage: (NSNumber *) page
                           handler: (void (^)(NSArray *repositoryArray)) completionHandler;
- (void) getRepositoryPullsWithPage: (NSNumber *) page
                       repoFullName: (NSString *) repoFullName
                            handler: (void (^)(NSArray *repositoryArray)) completionHandler;

@end
