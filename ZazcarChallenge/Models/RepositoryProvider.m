//
//  RepositoryProvider.m
//  ZazcarChallenge
//
//  Created by Gabriel Andreatto on 2/19/16.
//  Copyright © 2016 Gabriel Andreatto. All rights reserved.
//

#import "RepositoryProvider.h"
#import "RepositoryModel.h"
#import "RepositoryArrayModel.h"
#import "RepositoryPullModel.h"
#import "HTTPRequest.h"

#import <Motis/Motis.h>

@implementation RepositoryProvider

- (void) getRepositoryListWithPage: (NSNumber *) page
                                 handler: (void (^)(NSArray *repositoryArray)) completionHandler {
    HTTPRequest *request = [[HTTPRequest alloc] init];
    
    NSDictionary *parameters = @{@"q": @"language:Java", @"sort": @"stars", @"page": page};
    
    [request makeGETRequestWithStringUrl: @"search/repositories"
                              parameters: parameters
                       completionHandler: ^ (id response) {
                           RepositoryArrayModel *repositoryArrayModel = [[RepositoryArrayModel alloc] init];
                           [repositoryArrayModel mts_setValuesForKeysWithDictionary:response];
                           
                           completionHandler(repositoryArrayModel.repositoryArray);
                       }];
}

- (void) getRepositoryPullsWithPage: (NSNumber *) page
                       repoFullName: (NSString *) repoFullName
                           handler: (void (^)(NSArray *repositoryArray)) completionHandler {
    HTTPRequest *request = [[HTTPRequest alloc] init];
    
    NSDictionary *parameters = @{@"page": page};
    
    [request makeGETRequestWithStringUrl: [@"repos/" stringByAppendingString: [repoFullName stringByAppendingString:@"/pulls"]]
                              parameters: parameters
                       completionHandler: ^ (id response) {
                           NSArray *repositoryPullsArray = [self parseJSONArray:response];
                           completionHandler(repositoryPullsArray);
                       }];
}

- (NSArray *) parseJSONArray: (NSArray *) jsonArray {
    
    NSMutableArray *repositoryPullArray = [NSMutableArray array];
    for (NSDictionary *dict in jsonArray)
    {
        RepositoryPullModel *repositoryPullModel = [[RepositoryPullModel alloc] init];
        [repositoryPullModel mts_setValuesForKeysWithDictionary:dict];

        [repositoryPullArray addObject:repositoryPullModel];
    }
    
    return repositoryPullArray;
}

@end
