//
//  RepositoryArrayModel.h
//  ZazcarChallenge
//
//  Created by Gabriel Andreatto on 2/20/16.
//  Copyright © 2016 Gabriel Andreatto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RepositoryArrayModel : NSObject

@property (nonatomic, strong) NSArray *repositoryArray;

@end
