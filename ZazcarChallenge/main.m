//
//  main.m
//  ZazcarChallenge
//
//  Created by Gabriel Andreatto on 2/18/16.
//  Copyright © 2016 Gabriel Andreatto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
