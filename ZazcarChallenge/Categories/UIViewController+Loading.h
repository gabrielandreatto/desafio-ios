//
//  UIViewController+Loading.h
//  ZazcarChallenge
//
//  Created by Gabriel Andreatto on 2/21/16.
//  Copyright © 2016 Gabriel Andreatto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface UIViewController (Loading)

@property (nonatomic, strong) UIView *activityView;

- (void)showActivityViewer;
- (void)hideActivityViewer;

@end
