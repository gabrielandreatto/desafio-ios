//
//  UIViewController+Loading.m
//  ZazcarChallenge
//
//  Created by Gabriel Andreatto on 2/21/16.
//  Copyright © 2016 Gabriel Andreatto. All rights reserved.
//

#import "UIViewController+Loading.h"
#import <objc/runtime.h>

@implementation UIViewController (Loading)

@dynamic activityView;

- (void)showActivityViewer {
    [NSThread detachNewThreadSelector:@selector(showActivityViewerAnotherThread) toTarget:self withObject:nil];
}

- (void)showActivityViewerAnotherThread {
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    UIWindow *window = delegate.window;
    
    UIView *_activityView = objc_getAssociatedObject(self, @selector(activityView));
    _activityView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, window.bounds.size.width, window.bounds.size.height)];
    _activityView.backgroundColor = [UIColor grayColor];
    _activityView.alpha = 0.4;
    
    UIActivityIndicatorView *activityWheel = [[UIActivityIndicatorView alloc] initWithFrame: CGRectMake(window.bounds.size.width / 2 - 12, window.bounds.size.height / 2 - 12, 24, 24)];
    activityWheel.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    activityWheel.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
                                      UIViewAutoresizingFlexibleRightMargin |
                                      UIViewAutoresizingFlexibleTopMargin |
                                      UIViewAutoresizingFlexibleBottomMargin);
    [_activityView addSubview:activityWheel];
    [window addSubview: _activityView];
    
    [[[_activityView subviews] objectAtIndex:0] startAnimating];
    
    objc_setAssociatedObject(self, @selector(activityView), _activityView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)hideActivityViewer {
    UIView *_activityView = objc_getAssociatedObject(self, @selector(activityView));
    
    [[[_activityView subviews] objectAtIndex:0] stopAnimating];
    [_activityView removeFromSuperview];
    _activityView = nil;
    
    objc_setAssociatedObject(self, @selector(activityView), _activityView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
