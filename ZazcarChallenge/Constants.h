//
//  Constants.h
//  ZazcarChallenge
//
//  Created by Gabriel Andreatto on 2/19/16.
//  Copyright © 2016 Gabriel Andreatto. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

extern NSString * const HOST_URL;

@end
