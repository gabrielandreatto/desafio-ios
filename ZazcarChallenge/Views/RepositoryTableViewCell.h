//
//  ProjectTableViewCell.h
//  ZazcarChallenge
//
//  Created by Gabriel Andreatto on 2/18/16.
//  Copyright © 2016 Gabriel Andreatto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RepositoryTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *userAvatar;
@property (nonatomic, strong) IBOutlet UILabel *username;
@property (nonatomic, strong) IBOutlet UILabel *repo_name;
@property (nonatomic, strong) IBOutlet UILabel *repo_description;
@property (nonatomic, strong) IBOutlet UILabel *forks_count;
@property (nonatomic, strong) IBOutlet UILabel *stargazers_count;


@end
