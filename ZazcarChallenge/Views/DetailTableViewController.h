//
//  DetailTableViewController.h
//  ZazcarChallenge
//
//  Created by Gabriel Andreatto on 2/20/16.
//  Copyright © 2016 Gabriel Andreatto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailTableViewController : UITableViewController

@property (nonatomic, strong) NSString *repositoryFullName;
@property (nonatomic, strong) NSString *repositoryName;

@end
