//
//  DetailTableViewController.m
//  ZazcarChallenge
//
//  Created by Gabriel Andreatto on 2/20/16.
//  Copyright © 2016 Gabriel Andreatto. All rights reserved.
//

#import "DetailTableViewController.h"
#import "DetailTableViewCell.h"
#import "RepositoryProvider.h"
#import "RepositoryPullModel.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import <UIScrollView+SVPullToRefresh.h>
#import <UIScrollView+SVInfiniteScrolling.h>

#import "UIViewController+Loading.h"


@interface DetailTableViewController ()

@property (nonatomic, strong) RepositoryProvider *repositoryProvider;
@property (nonatomic, strong) NSMutableArray *repositoryPullArray;
@property (nonatomic, strong) UIView *activityView;
@property (nonatomic, strong) NSNumber *page;

@end


@implementation DetailTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _repositoryProvider = [[RepositoryProvider alloc] init];
    _repositoryPullArray = [[NSMutableArray alloc] init];
    _page = @1;
    
    self.navigationItem.title = _repositoryName;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [self fetchData];
    [self configInfiniteScroll];
    [self configPullToRefresh];
}

#pragma mark - Fetch data
- (void) configInfiniteScroll {
    DetailTableViewController * __weak weakSelf = self;
    
    [self.tableView addInfiniteScrollingWithActionHandler: ^{
        weakSelf.page = [NSNumber numberWithInt:([weakSelf.page intValue] + 1)];
        [weakSelf fetchDataWithPage: weakSelf.page handler: ^{
            [weakSelf.tableView.infiniteScrollingView stopAnimating];
        }];
    }];
}

- (void) configPullToRefresh {
    DetailTableViewController * __weak weakSelf = self;
    
    [self.tableView addPullToRefreshWithActionHandler: ^{
        weakSelf.page = @1;
        
        [weakSelf fetchDataWithPage: weakSelf.page handler: ^{
            weakSelf.repositoryPullArray = [[NSMutableArray alloc] init];
            [weakSelf.tableView.pullToRefreshView stopAnimating];
        }];
    }];
}

- (void) fetchData {
    DetailTableViewController * __weak weakSelf = self;
    
    [self showActivityViewer]; 
    [self fetchDataWithPage:_page handler: ^{
        [weakSelf hideActivityViewer];
    }];
}

- (void) fetchDataWithPage:(NSNumber *) page handler: (void (^)(void)) completionHandler {
    DetailTableViewController * __weak weakSelf = self;
    
    [_repositoryProvider getRepositoryPullsWithPage:_page
                                       repoFullName:_repositoryFullName
                                           handler:^(NSArray *repostoryArray) {
                                               if(completionHandler) completionHandler();
                                               
                                               [_repositoryPullArray addObjectsFromArray:repostoryArray];
                                               [weakSelf.tableView reloadData];
                                           }];
}

#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 155;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _repositoryPullArray.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    RepositoryPullModel *repositoryPullModel = _repositoryPullArray[indexPath.row];
    [[UIApplication sharedApplication] openURL: repositoryPullModel.pull_request_url];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"detailTableCell";
    
    DetailTableViewCell *cell = [tableView
                                     dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[DetailTableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:cellIdentifier];
    }

    if (_repositoryPullArray.count > indexPath.row) {
        RepositoryPullModel *repositoryPullModel = _repositoryPullArray[indexPath.row];
        
        // Configure the cell
        cell.pull_request_name.text = repositoryPullModel.pull_request_name;
        cell.pull_request_description.text = repositoryPullModel.pull_request_description;
        cell.username.text = repositoryPullModel.username;
        [cell.userAvatar sd_setImageWithURL: repositoryPullModel.userAvatar];
    }

    
    return cell;
}

@end
