//
//  ProjectsTableViewController.m
//  ZazcarChallenge
//
//  Created by Gabriel Andreatto on 2/18/16.
//  Copyright © 2016 Gabriel Andreatto. All rights reserved.
//

#import "RepositoryTableViewController.h"
#import "RepositoryTableViewCell.h"
#import "RepositoryProvider.h"
#import "RepositoryModel.h"
#import "DetailTableViewController.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import <UIScrollView+SVPullToRefresh.h>
#import <UIScrollView+SVInfiniteScrolling.h>

#import "UIViewController+Loading.h"


@interface RepositoryTableViewController ()

@property (nonatomic, strong) NSMutableArray *repositoryArray;
@property (nonatomic, strong) RepositoryProvider *repositoryProvider;
@property (nonatomic, strong) NSNumber *page;

@end


@implementation RepositoryTableViewController

@synthesize repositoryArray = _repositoryArray;
@synthesize repositoryProvider = _repositoryProvider;
@synthesize page = _page;

- (void)viewDidLoad {
    [super viewDidLoad];
    _repositoryProvider = [[RepositoryProvider alloc] init];
    _repositoryArray = [[NSMutableArray alloc] init];
    _page = @1;
    
    [self fetchData];
    [self configInfiniteScroll];
    [self configPullToRefresh];
}

#pragma mark - Fetch data
- (void) configInfiniteScroll {
    RepositoryTableViewController * __weak weakSelf = self;
    
    [self.tableView addInfiniteScrollingWithActionHandler: ^{
        weakSelf.page = [NSNumber numberWithInt:([weakSelf.page intValue] + 1)];
        [weakSelf fetchDataWithPage: weakSelf.page handler: ^{
            [weakSelf.tableView.infiniteScrollingView stopAnimating];
        }];
    }];
}

- (void) configPullToRefresh {
    RepositoryTableViewController * __weak weakSelf = self;
    
    [self.tableView addPullToRefreshWithActionHandler: ^{
        weakSelf.page = @1;
        
        [weakSelf fetchDataWithPage: weakSelf.page handler: ^{
            weakSelf.repositoryArray = [[NSMutableArray alloc] init];
            [weakSelf.tableView.pullToRefreshView stopAnimating];
        }];
    }];
}

- (void) fetchData {
    RepositoryTableViewController * __weak weakSelf = self;
    
    [self showActivityViewer];
    [self fetchDataWithPage:_page handler: ^{
        [weakSelf hideActivityViewer];
    }];
}

- (void) fetchDataWithPage:(NSNumber *) page handler: (void (^)(void)) completionHandler {
    RepositoryTableViewController * __weak weakSelf = self;
    
    [_repositoryProvider getRepositoryListWithPage:page
                                           handler:^(NSArray *repostoryArray) {
        if(completionHandler) completionHandler();
                                               
        [_repositoryArray addObjectsFromArray:repostoryArray];
        [weakSelf.tableView reloadData];
    }];
}

#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 135;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _repositoryArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"repositoryTableCell";
    
    RepositoryTableViewCell *cell = [tableView
                              dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[RepositoryTableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:cellIdentifier];
    }
    
    RepositoryModel *repositoryModel = _repositoryArray[indexPath.row];
    
    // Configure the cell
    cell.repo_description.text = repositoryModel.repo_description;
    cell.repo_name.text = repositoryModel.repo_name;
    cell.forks_count.text = repositoryModel.forks_count;
    cell.stargazers_count.text = repositoryModel.stargazers_count;
    
    cell.username.text = repositoryModel.username;
    [cell.userAvatar sd_setImageWithURL: repositoryModel.userAvatar];
    
    
    return cell;
}

#pragma mark - Detail View
- (void) prepareForSegue:(UIStoryboardSegue *) segue sender:(id) sender {
    if ([[segue identifier] isEqualToString:@"ShowRepositoryDetails"]) {
        DetailTableViewController *detailViewController = [segue destinationViewController];
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        RepositoryModel *repositoryModel = _repositoryArray[indexPath.row];
        
        detailViewController.repositoryFullName = repositoryModel.repo_full_name;
        detailViewController.repositoryName = repositoryModel.repo_name;
    }
}

@end
