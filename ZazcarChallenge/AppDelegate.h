//
//  AppDelegate.h
//  ZazcarChallenge
//
//  Created by Gabriel Andreatto on 2/18/16.
//  Copyright © 2016 Gabriel Andreatto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

